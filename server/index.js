const express = require("express");

const cors = require("cors");
const passport = require("passport");

const app = express();
require("dotenv").config();
const db = require("./config/db");

require("./Models/userModel");
require("./config/passport")(passport);
app.use(passport.initialize());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
const userApis = require("./Apis/userApis");
const categoriesApis = require("./Apis/EducationApis/categoryApis");
const courseApis = require("./Apis/EducationApis/courseApi");
app.use("/users", userApis);
app.use("/categories", [categoriesApis, courseApis]);
app.use("/help", require("./Apis/helpApis/helpApis"));

app.listen(7300, () =>
  console.log("server connected on http://localhost:7300/")
);
