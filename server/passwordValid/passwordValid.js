const bcrypt = require("bcrypt");
const jsonwebtoken = require("jsonwebtoken");
const fs = require("fs");
const path = require("path");

const pathTokey = path.join(__dirname, "..", "/genKeys", "/id_rsa_priv.pem");
const PRIV_KEY = fs.readFileSync(pathTokey, "utf8");
const pathToKey = path.join(__dirname, "..", "/genKeys", "/id_rsa_pub.pem");
const PUB_KEY = fs.readFileSync(pathToKey, "utf8");

function validPassword(password, hash) {
  return bcrypt.compareSync(password, hash);
}

function genPassword(password, response) {
  try {
    const saltRounds = 10;
    var salt = bcrypt.genSaltSync(saltRounds);
    const hash = bcrypt.hashSync(password, salt);

    return hash;
  } catch (e) {
    return response.json("enter a password");
  }
}

function issueJWT(user) {
  const _id = user._id;

  const expiresIn = "1d";

  const payload = {
    sub: _id,
    iat: Date.now(),
  };

  const signedToken = jsonwebtoken.sign(payload, PRIV_KEY, {
    expiresIn: expiresIn,
    algorithm: "RS256",
  });

  return {
    token: "Bearer " + signedToken,
    expires: expiresIn,
  };
}

var validateInputPassword = (password) => {
  var re = /^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/;
  return re.test(password);
};

module.exports.validPassword = validPassword;
module.exports.genPassword = genPassword;
module.exports.issueJWT = issueJWT;
module.exports.validateInputPassword = validateInputPassword;
