const mongoose = require("mongoose");
const BugSchema = new mongoose.Schema({
  subject: { type: String, required: true },
  sender: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "users",
  },
  description: { type: String, required: true },
  date: { type: Date },
});

module.exports = mongoose.model("bug", BugSchema);
