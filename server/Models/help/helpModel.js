const mongoose = require("mongoose");
const helpSchema = new mongoose.Schema({
  subject: { type: String, required: true },
  sender: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "users",
  },
  description: { type: String, required: true },
  mediacontent: { type: String },
  date: { type: Date },
});

module.exports = mongoose.model("help", helpSchema);
