const mongoose = require("mongoose");

var validateEmail = function (email) {
  var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
};

var validatePhone = (phone) => {
  re = `${phone}`;
  return re.length == 8;
};

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    validate: [validateEmail, "Please fill a valid email address"],
  },
  phone: {
    type: Number,
    minlength: 8,
    maxlength: 8,
    validate: {
      validator: Number.isInteger,
      message: "{VALUE} is not an integer value",
    },
    validate: {
      validator: validatePhone,
      message: " not a valid phone number",
    },
    unique: true,
  },

  password: {
    type: String,
    required: true,
  },
  role: { type: String, enum: ["student, instructor"] },
  points: { type: Number },
  img: { type: String },
});

module.exports = mongoose.model("users", userSchema);
