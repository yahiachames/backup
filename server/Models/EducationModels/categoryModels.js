const mongoose = require("mongoose");
const CategorySchema = new mongoose.Schema({
  title: { type: String, required: true, unique: true },
  description: { type: String, required: true },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "users",
  },
  img: { type: String, required: true },
  courses: [{ type: mongoose.Schema.Types.ObjectId, ref: "course" }],
});

module.exports = mongoose.model("category", CategorySchema);
