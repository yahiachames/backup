const mongoose = require("mongoose");
const TopicSchema = new mongoose.Schema({
  title: { type: String, required: true },
  typeofcontent: {
    type: String,
    enum: ["paragraph", "quiz", "img", "video", "game", "task"],
    required: true,
  },
  contentmedia: { type: String },
  contentstring: { type: String },
  author: { type: String, required: true },
});

module.exports = mongoose.model("topic", TopicSchema);
