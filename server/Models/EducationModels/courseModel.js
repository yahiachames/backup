const mongoose = require("mongoose");
const CourseSchema = new mongoose.Schema({
  title: { type: String, required: true },
  description: { type: String, required: true },
  author: { type: String, required: true },
  img: { type: String, required: true },
  topics: [{ type: mongoose.Schema.Types.ObjectId, ref: "topic" }],
});

module.exports = mongoose.model("course", CourseSchema);
