const app = require("express")();
const router = require("express").Router();
const Category = require("../../Models/EducationModels/categoryModels");
const Course = require("../../Models/EducationModels/courseModel");
const passport = require("passport");
const topicsRouter = require("./topicApis");

router.post(
  "/acourse",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    var course = new Course({
      title: req.body.title,
      description: req.body.description,
      author: req.body.author,

      img: req.body.img,
    });
    course.save({}, (err, result) => {
      if (err) res.json({ success: false, msg: err });
      Category.findByIdAndUpdate(
        req.body.category_id,
        { $addToSet: { courses: result._id } },
        (err2, result2) => {
          if (err2) res.json({ success: false, err: err2 });
          res.json({
            success: true,
            resultCateg: result2,
            resultCourse: result,
          });
        }
      );
    });
  }
);

router.get(
  "/gallcourse",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Category.findById(req.body.category_id)
      .populate("courses")
      .then((result) => res.json({ success: true, result }))
      .catch((e) => res.json({ success: false, err: e }));
  }
);

router.get(
  "/gonecourse",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Category.findById(req.body.category_id)
      .populate("courses")
      .select("courses")
      .then((result) => {
        var course = result.courses.find((el) => el._id == req.body.course_id);

        res.json({
          course,
          success: true,
        });
      })
      .catch((e) => res.json({ success: false, err: e }));
  }
);

router.post(
  "/dcourse",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Category.findByIdAndUpdate(req.body.category_id, {
      $pull: { courses: req.body.course_id },
    })
      .populate("courses")
      .select("courses")
      .then((result) => {
        Course.findByIdAndDelete(req.body.course_id, (err, result2) => {
          if (err) res.json({ success: false, err: err });
          res.json({ success: true, result: result2 });
        });
      })
      .catch((e) => res.send(e));
  }
);

router.post(
  "/ucourse",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Course.findById(req.body.course_id)
      .then((result) => {
        var input = {
          title: req.body.title ? req.body.title : result.title,
          description: req.body.description
            ? req.body.description
            : result.description,
          img: req.body.img ? req.body.img : result.img,
        };

        Course.update(
          { _id: result._id },
          {
            $set: {
              title: input.title,
              description: input.description,
              img: input.img,
            },
          },
          (err, result2) => {
            if (err) res.json({ success: false, msg: err });
            else res.json({ success: true, msg: result2 });
          }
        );
      })
      .catch((e) => res.json({ success: false, msg: e }));
  }
);

module.exports = app.use("/courses", [router, topicsRouter]);
