const app = require("express")();
const mongoose = require("mongoose");
const router = require("express").Router();
const Topic = require("../../Models/EducationModels/topicModel");
const Course = require("../../Models/EducationModels/courseModel");
const passport = require("passport");
const path = require("path");

const multer = require("multer");

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, "..", "..", "uploads"));
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname + "-" + Date.now());
  },
});

function fileFilter(req, file, cb) {
  if (
    file.mimetype == "image/png" ||
    file.mimetype == "image/jpeg" ||
    file.mimetype == "video/mp4"
  ) {
    cb(null, true);
  }
  // To accept the file pass `true`, like so:
  else {
    cb(new Error("only jpeg , png and mp4 media"), false);
  }
}
var upload = multer({ storage, fileFilter: fileFilter });
router.post(
  "/atopic",
  passport.authenticate("jwt", { session: false }),
  upload.single("file"),
  (req, res) => {
    console.log(res.req.body);
    let topic = new Topic({
      title: req.body.title,
      typeofcontent: req.body.typeofcontent,
      contentstring: req.body.contentstring,
      author: req.body.author,
      contentmedia: res.req.file.filename,
    });
    topic
      .save()
      .then((result) => {
        Course.findByIdAndUpdate(
          req.body.course_id,
          { $addToSet: { topics: result._id } },
          (err2, result2) => {
            if (err2) res.json({ success: false, err: err2 });
            res.json({
              success: true,
              resultcourse: result2,
              resulttopic: result,
            });
          }
        );
      })
      .catch((e) => res.json({ success: false, msg: e }));
  }
);

router.get(
  "/gonetopic",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Topic.findById({ _id: req.body.topic_id }, (err, result) => {
      if (err) res.send(err);
      res.send(result);
    });
  }
);

router.get(
  "/galltopic",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Course.findById(req.body.course_id)
      .select("topics")
      .populate("topics")
      .then((result) => res.send(result))
      .catch((e) => res.send(e));
  }
);

router.post(
  "/dtopic",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Course.findByIdAndUpdate(req.body.course_id, {
      $pull: { topics: req.body.topic_id },
    })
      .populate("topics")
      .select("topics")
      .then((result) => {
        Topic.findByIdAndDelete(req.body.topic_id, (err, result2) => {
          if (err) res.json({ success: false, err: err });
          res.json({ success: true, result: result2 });
        });
      })
      .catch((e) => res.send(e));
  }
);

router.post(
  "/utopic",
  passport.authenticate("jwt", { session: false }),
  upload.single("file"),
  (req, res) => {
    Topic.findById(req.body.topic_id)
      .then((result) => {
        var input = {
          title: req.body.title ? req.body.title : result.title,
          typeofcontent: req.body.typeofcontent
            ? req.body.typeofcontent
            : result.typeofcontent,
          contentmedia:
            req.file !== undefined ? req.file.filename : result.contentmedia,
        };

        Topic.updateOne(
          { _id: result._id },
          {
            $set: {
              title: input.title,
              typeofcontent: input.typeofcontent,
              contentmedia: input.contentmedia,
            },
          },
          (err, result2) => {
            if (err) res.json({ success: false, ERRfromupdate: err });
            res.json({ success: true, ERRfromupdate: result2 });
          }
        );
      })
      .catch((e) => res.json({ success: false, msgfromfind: e }));
  }
);

module.exports = app.use("/topics", router);
