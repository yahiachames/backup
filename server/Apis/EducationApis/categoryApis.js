const router = require("express").Router();
const Category = require("../../Models/EducationModels/categoryModels");
const passport = require("passport");

router.post(
  "/acategory",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    var category = new Category({
      title: req.body.title,
      description: req.body.description,
      author: req.body.author,

      img: req.body.img,
    });
    category.save({}, (err, result) => {
      if (err) res.json({ success: false, msg: err });
      res.json({ sucess: true, msg: result });
    });
  }
);
router.get(
  "/gallcategory",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Category.find({}, (err, result) => {
      if (err) res.json({ success: false, msg: err });
      res.json({ sucess: true, msg: result });
    });
  }
);

router.post(
  "/dcategory",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Category.findOneAndDelete({ _id: req.body._id }, (err, result) => {
      if (err) res.json({ success: false, msg: `${err}` });
      res.json({ sucess: true, msg: `${result}` });
    });
  }
);

router.post(
  "/ucategory",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Category.findById(req.body._id)
      .then((result) => {
        var input = {
          title: req.body.title ? req.body.title : result.title,
          description: req.body.description
            ? req.body.description
            : result.description,
          img: req.body.img ? req.body.img : result.img,
        };

        Category.update(
          { _id: result._id },
          {
            $set: {
              title: input.title,
              description: input.description,
              img: input.img,
            },
          },
          (err, result2) => {
            if (err) res.json({ success: false, msg: err });
            else res.json({ success: true, msg: result2 });
          }
        );
      })
      .catch((e) => res.json({ success: false, msg: e }));
  }
);

router.get(
  "/gonecategory",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Category.findById(req.body._id, (err, result) => {
      if (err) res.json({ success: false, msg: err });
      res.json({ sucess: true, msg: result });
    });
  }
);

module.exports = router;
