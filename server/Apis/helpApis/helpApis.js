const router = require("express").Router();
const Help = require("../../Models/help/helpModel");
const multer = require("multer");
const path = require("path");
const passport = require("passport");

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, "..", "..", "uploads"));
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname + "-" + Date.now());
  },
});

function fileFilter(req, file, cb) {
  if (
    file.mimetype == "image/png" ||
    file.mimetype == "image/jpeg" ||
    file.mimetype == "video/mp4"
  ) {
    cb(null, true);
  }
  // To accept the file pass `true`, like so:
  else {
    cb(new Error("only jpeg , png and mp4 media"), false);
  }
}
var upload = multer({ storage, fileFilter: fileFilter });

router.post(
  "/ahelp",
  passport.authenticate("jwt", { session: false }),
  upload.single("file"),
  (req, res) => {
    var help = new Help({
      subject: req.body.subject,
      description: req.body.description,
      sender: req.body.sender,
      date: Date.now(),
      mediacontent: req.file.filename,
    });
    help.save({}, (err, result) => {
      if (err) res.json({ success: false, msg: err });
      res.json({ sucess: true, msg: result });
    });
  }
);
router.get(
  "/gonehelp",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Help.findOne({ subject: req.body.subject })
      .populate("sender")

      .then((result) => {
        res.json({
          success: true,
          result: {
            subject: result.subject,
            description: result.description,
            sender: result.sender.username,
            sender_id: result.sender._id,
          },
        });
      })
      .catch((e) => res.json({ success: false, err: e }));
  }
);

router.get(
  "/gallhelp",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Help.find({}, (err, result) => {
      if (err) res.json({ success: false, err: err });
      res.json({ success: true, result: result });
    });
  }
);

router.post(
  "/dhelp",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Help.findByIdAndDelete(req.body.id, (err, result) => {
      if (err) res.json({ success: false, err: err });
      res.json({ success: true, result: result });
    });
  }
);

module.exports = router;
