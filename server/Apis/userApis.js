const router = require("express").Router();
const User = require("../Models/userModel");
const passport = require("passport");

const genPassword = require("../passwordValid/passwordValid");

router.get("/login", (req, res) => {
  User.findOne({ username: req.body.username }, (err, result) => {
    if (err) res.send(err);
    if (result) {
      var isValid = genPassword.validPassword(
        req.body.password,
        result.password
      );

      if (!result.username || isValid === false) {
        res
          .status(401)
          .json({ success: false, msg: "username or password incorrect" });
      } else {
        let filterResult = {
          _id: result._id,
          username: result.username,
          email: result.email,
          phone: result.phone,
        };
        let tokenObject = genPassword.issueJWT(filterResult);

        res.status(200).json({
          success: true,
          user: filterResult,
          token: tokenObject.token,
          expiresIn: tokenObject.expires,
        });
      }
    }
  });
});

router.post("/register", (req, res) => {
  if (
    (genPassword.validateInputPassword(req.body.password) == false) &
    req.body.password
  ) {
    res.json({
      PasswordValidator: false,
      msg:
        "password length more than 8 characters which contain at least one numeric digit, one uppercase , one lowercase letter and a special character",
    });
  }

  const hash = genPassword.genPassword(req.body.password, res);

  user = new User({
    username: req.body.username,
    email: req.body.email,
    phone: req.body.phone,
    password: hash,
  });
  user.save({}, (err, result) => {
    if (err) {
      if (err.errmsg) {
        if (
          err.errmsg.indexOf(
            "E11000 duplicate key error collection: backup.users index: username_1 dup key"
          ) !== -1
        )
          res.json({
            typeError: "username duplicate",
            errmsg: "this username already exist",
          });
        else if (
          err.errmsg.indexOf(
            "E11000 duplicate key error collection: backup.users index: email_1 dup key"
          ) !== -1
        )
          res.json({
            typeError: "email duplicate",
            errmsg: "this email already exist",
          });
        else if (
          err.errmsg.indexOf(
            "E11000 duplicate key error collection: backup.users index: phone_1 dup key:"
          ) !== -1
        )
          res.json({
            typeError: "phone duplicate",
            errmsg: "this phone already exist",
          });
        else {
          res.json(err.errmsg);
        }
      } else res.send({ err: err });
    }
    let tokenObject = genPassword.issueJWT(result);
    const user = {
      username: result.username,
      email: result.email,
      points: result.points ? result.points : null,
      role: result.role ? result.role : null,
      img: result.img ? result.img : null,
    };
    res.status(200).json({
      success: true,
      user: user,
      token: tokenObject.token,
      expiresIn: tokenObject.expires,
    });
  });
});

router.get(
  "/getusers",
  passport.authenticate("jwt", { session: false }),

  (req, res) => {
    User.find({}, (err, result) => {
      if (err) res.send(err);
      res.send(result);
    }).select("username email phone");
  }
);

router.get(
  "/:id/getuser",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    User.findById({ _id: req.params.id }, (err, result) => {
      if (err) res.send(err);
      res.send(result);
    }).select("username email phone");
  }
);

module.exports = router;
