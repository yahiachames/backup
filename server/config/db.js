const mongoose = require("mongoose");

mongoose.connect(
  process.env.MONGODB,
  { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false },
  () => console.log("db working")
);

var db = mongoose.connection;
module.exports = db;
