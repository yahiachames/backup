import React from "react";
import LoginCmp from "./loginPage/LogingCmp";
import "./App.css";

function App() {
  return (
    <div className='App'>
      <LoginCmp />
    </div>
  );
}

export default App;
